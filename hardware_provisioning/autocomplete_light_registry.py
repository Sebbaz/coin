from autocomplete_light import shortcuts as al
from .models import Item


# This will generate a MemberAutocomplete class
al.register(Item,
                            # Just like in ModelAdmin.search_fields
                            search_fields=[
                                'designation', 'mac_address', 'serial'],
                            attrs={
                                # This will set the input placeholder attribute:
                                'placeholder': "Nom / adresse MAC / n° de série de l'objet",
                                'data-autocomplete-minimum-characters': 3,
                            },
)
