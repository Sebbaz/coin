from .settings_base import *

# settings for unit tests

EXTRA_INSTALLED_APPS = (
    'hardware_provisioning',
    'maillists',
    'vpn',
    'vps',
    'housing',
)

TEMPLATES[0]['DIRS'] = EXTRA_TEMPLATE_DIRS + TEMPLATES[0]['DIRS']
INSTALLED_APPS = INSTALLED_APPS + EXTRA_INSTALLED_APPS
