"""
Configuration coin pour la CI
"""
from coin.settings_test import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'coin',
        'USER': 'coin',
        'PASSWORD': 'coin',
        'HOST': 'postgres',  # Empty for localhost through domain sockets
        'PORT': '5432',      # Empty for default
        'ATOMIC_REQUESTS': True,
    }
}
