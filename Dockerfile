FROM python:3.7-buster

ENV DEBIAN_FRONTEND noninteractive
ENV TZ="Europe/Paris"
ENV LC_ALL fr_FR.UTF-8

RUN --mount=type=cache,sharing=locked,target=/var/cache/apt \
    --mount=type=cache,sharing=locked,target=/var/lib/apt \
    apt-get update \
    && apt-get install -y --no-install-recommends libsasl2-dev libldap2-dev libssl-dev

WORKDIR /coin
COPY requirements.txt .
RUN --mount=type=cache,sharing=locked,target=/root/.cache \
    pip install -U pip \
 && pip install -r requirements.txt

COPY . .
RUN useradd --uid 10001 --user-group --shell /bin/bash coin
RUN chown coin:coin /coin

USER coin:coin

VOLUME ["/coin"]
