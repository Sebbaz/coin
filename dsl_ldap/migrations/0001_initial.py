from django.db import models, migrations
import coin.mixins
import ldapdb.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('configuration', '0003_configuration_comment'),
    ]

    operations = [
        migrations.CreateModel(
            name='LdapDSLConfig',
            fields=[
                ('dn', models.CharField(max_length=200)),
                ('login', ldapdb.models.fields.CharField(max_length=255, serialize=False, primary_key=True, db_column=b'cn')),
                ('password', ldapdb.models.fields.CharField(max_length=255, db_column=b'userPassword')),
                ('cleartext_password', ldapdb.models.fields.CharField(max_length=255, db_column=b'description')),
                ('active', ldapdb.models.fields.CharField(max_length=3, db_column=b'dialupAccess')),
                ('ipv4_endpoint', ldapdb.models.fields.CharField(max_length=16, db_column=b'radiusFramedIPAddress')),
                ('ranges_v4', ldapdb.models.fields.ListField(db_column=b'radiusFramedRoute')),
                ('ranges_v6', ldapdb.models.fields.ListField(db_column=b'ipHostNumber')),
            ],
            options={
                'managed': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DSLConfiguration',
            fields=[
                ('configuration_ptr', models.OneToOneField(parent_link=True,
                    auto_created=True, primary_key=True, serialize=False,
                    to='configuration.Configuration', on_delete=models.CASCADE)),
                ('phone_number', models.CharField(help_text='Phone number associated to the DSL line', max_length=20, verbose_name='phone number')),
                ('activated', models.BooleanField(default=False, verbose_name='activated')),
                ('login', models.CharField(help_text='Leave empty for automatic generation', unique=True, max_length=50, verbose_name='login')),
                ('password', models.CharField(help_text='Will be stored in cleartext!', max_length=256, verbose_name='password')),
            ],
            options={
                'verbose_name': 'DSL configuration',
                'verbose_name_plural': 'DSL configurations',
            },
            bases=(coin.mixins.CoinLdapSyncMixin, 'configuration.configuration'),
        ),
        migrations.CreateModel(
            name='RadiusGroup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=256, verbose_name='group name')),
                ('realm', models.CharField(help_text='Example: "fdn.nerim"', max_length=50, verbose_name='radius realm')),
                ('suffix', models.CharField(help_text='Optional suffix added to the login, as a kind of "sub-realm".  Example: "%gnd"', max_length=50, verbose_name='suffix', blank=True)),
                ('comment', models.CharField(max_length=256, verbose_name='comment', blank=True)),
            ],
            options={
                'verbose_name': 'radius group',
                'verbose_name_plural': 'radius groups',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='dslconfiguration',
            name='radius_group',
            field=models.ForeignKey(verbose_name='radius group',
                to='dsl_ldap.RadiusGroup', help_text='Group (i.e. backhaul) to use',
                on_delete=models.CASCADE),
            preserve_default=True,
        ),
    ]
